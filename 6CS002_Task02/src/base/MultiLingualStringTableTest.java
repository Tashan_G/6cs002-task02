package base;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class MultiLingualStringTableTest {

    @Test
    void testExtractedMethod() {

        assertEquals("Enter your name:", MultiLingualStringTable.getMessage(0));

        assertEquals("Enter your name:", getMessageFromLanguageArray(MultiLingualStringTable.englishMessages, 0));
    }

    @Test
    void testReplaceMagicNumber() {

    }

    private String getMessageFromLanguageArray(String[] languageArray, int index) {
    	
        return MultiLingualStringTable.getMessageFromLanguageArray(languageArray, index);
    }
}
