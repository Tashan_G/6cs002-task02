package base;
/**
 * @author Kevan Buckley, maintained by __student
 * @version 2.0, 2014
 */

public class MultiLingualStringTable {
  private enum LanguageSetting {English, Klingon}
  private static LanguageSetting currentLangauge = LanguageSetting.English;
  static String [] englishMessages = {"Enter your name:", "Welcome", "Have a good time playing Abominodo"};
  private static String [] klinognMessages= {"'el lIj pong:", "nuqneH", "QaQ poH Abominodo"};
  
  /**
   * Gets the appropriate message based on the current language setting.
   *
   * @param index Index of the message to retrieve
   * @return The requested message in the current language
   */
  
  public static String getMessage(int index){
    if(currentLangauge == LanguageSetting.English ){
      return getMessageFromLanguageArray(englishMessages, index);
    } else {
    	return getMessageFromLanguageArray(klinognMessages, index);
    	}
    }
  
  /**
   * Retrieves a message from the given language array at the specified index.
   *
   * @param languageArray Array containing messages in a specific language
   * @param index         Index of the message to retrieve
   * @return The requested message from the language array
   */
    
  
    static String getMessageFromLanguageArray(String[] languageArray, int index) {
    	return languageArray[index];
    }
    
    public static void setLanguage(LanguageSetting language) {
        currentLangauge = language;
    
  }
}
