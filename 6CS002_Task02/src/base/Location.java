package base;

import java.awt.Color;
import java.awt.Graphics;
import java.io.BufferedReader;
import java.io.InputStreamReader;
/**
 * @author Kevan Buckley, maintained by __student
 * @version 2.0, 2014
 */

public class Location extends SpacePlace {
  public int columInt; // Initial Column
  public int rowInt; // Initial Row
  public DIRECTION directionInt; // Initial Direction
  public int tmporyInt; // Initial tempory value
  public enum DIRECTION {VERTICAL, HORIZONTAL};
  
  public Location(int r, int c) {
    this.rowInt = r;
    this.columInt = c;
  }

  public Location(int r, int c, DIRECTION d) {    
    this(r,c);
    this.directionInt=d;
  }
  
  public String toString() {
    if(directionInt==null){
      tmporyInt = columInt + 1;
      return "(" + (tmporyInt) + "," + (rowInt+1) + ")";
    } else {
      tmporyInt = columInt + 1;
      return "(" + (tmporyInt) + "," + (rowInt+1) + "," + directionInt + ")";
    }
  }
  
  public void drawGridLines(Graphics g) {
    g.setColor(Color.LIGHT_GRAY);
    for (tmporyInt = 0; tmporyInt <= 7; tmporyInt++) {
      g.drawLine(20, 20 + tmporyInt * 20, 180, 20 + tmporyInt * 20);
    }
    for (int see = 0; see <= 8; see++) {
      g.drawLine(20 + see * 20, 20, 20 + see * 20, 160);
    }
  }
  
  public static int getInt() {
    BufferedReader r = new BufferedReader(new InputStreamReader(System.in));
    do {
      try {
        return Integer.parseInt(r.readLine());
      } catch (Exception e) {
      }
    } while (true);
  }
}
