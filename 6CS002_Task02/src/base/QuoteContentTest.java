package base;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class QuoteContentTest {

	@Test
    public void testQuoteContent() {
        // Test data
        String[] testData = {
            "Progress comes from the intelligent use of experience.","Elbert Hubbard","",
            "No amount of experimentation can ever prove me right; a single experiment can prove me wrong.","Albert Einstein",""
        };

        // Create an instance of the renamed class
        QuoteContent quoteContent = new QuoteContent();

        // Set the test data to the static field
        QuoteContent.stuff = testData;

        // Perform assertions to test the functionality
        assertEquals("Progress comes from the intelligent use of experience.", quoteContent.stuff[0]);
        assertEquals("Elbert Hubbard", quoteContent.stuff[1]);
        assertEquals("", quoteContent.stuff[2]);
        assertEquals("No amount of experimentation can ever prove me right; a single experiment can prove me wrong.", quoteContent.stuff[3]);
        assertEquals("Albert Einstein", quoteContent.stuff[4]);
        assertEquals("", quoteContent.stuff[5]);
    }
}