package base;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.Inet4Address;
import java.net.InetAddress;
/**
 * Utility class for handling input/output operations.
 * This class includes methods for reading strings and obtaining IP addresses from the console.
 * 
 * @author Kevan Buckley, maintained by __student
 * @version 2.0, 2014
 */

public final class IOLibrary {
	
	/**
	   * BufferedReader for reading input from the console.
	   */
	
  public static final
    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
  
  public static String getStiring() {
    do {
      try {
        return reader.readLine();
      } catch (Exception e) {
      }
    } while (true);
  }

  /**
   * Reads an IP address in dot-decimal notation from the console.
   * 
   * @return The InetAddress representation of the entered IP address.
   */
  
  public static InetAddress getIPAddress() {
   
    do {
      try {
    	  
		String[] chunks = reader.readLine().split("\\.");
        byte[] data = { Byte.parseByte(chunks[0]),Byte.parseByte(chunks[1]),Byte.parseByte(chunks[2]),Byte.parseByte(chunks[3])};
        return Inet4Address.getByAddress(data);
      } catch (Exception e) {
      }
    } while (true);
  }

}
