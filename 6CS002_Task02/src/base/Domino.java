package base;
/**
 * @author Kevan Buckley, maintained by __student
 * @version 2.0, 2014
 */

public class Domino implements Comparable<Domino> {
  public int high;
  public int low;
  public int hx;
  public int hy;
  public int lx;
  public int ly;
  public boolean placed = false;
  
  /**

  Initializes a Domino object with given high and low values.
  @param high The upper value of the domino.
  @param low The lower value of the domino.
  */

  public Domino(int high, int low) {
    super();
    this.high = high;
    this.low = low;
  }
  
  public int getHigh() {
		return high;
	}
	public void setHigh(int high) {
		this.high = high;
	}
	
	/**
	 * Sets the position of the domino on the grid using specified coordinates.
	 * 
	 * @param hx The x-coordinate of the higher end.
	 * @param hy The y-coordinate of the higher end.
	 * @param lx The x-coordinate of the lower end.
	 * @param ly The y-coordinate of the lower end.
	 */

  
  public void place(int hx, int hy, int lx, int ly) {
    this.hx = hx;
    this.hy = hy;
    this.lx = lx;
    this.ly = ly;
    placed = true;
  }

  /**
   * Retrieves the string representation of the domino.
   * 
   * @return The string representation of the domino.
   */
  
  public String toString() {
    StringBuffer result = new StringBuffer();
    result.append("[");
    result.append(Integer.toString(high));
    result.append(Integer.toString(low));
    result.append("]");
    if(!placed){
      result.append("unplaced");
      appendUnplacedInfo(result);
    } else {
    	appendPlacedInfo(result);
    }
    return result.toString();
  }

  void appendPlacedInfo(StringBuffer result) {
	  result.append("unplaced");
	
}

void appendUnplacedInfo(StringBuffer result) {
	result.append("(");
	result.append(Integer.toString(getHx() + 1));
	result.append(",");
	result.append(Integer.toString(getHy() + 1));
	result.append(")");
	result.append("(");
	result.append(Integer.toString(getLx() + 1));
	result.append(",");
	result.append(Integer.toString(getLy() + 1));
	result.append(")");
}

public void setLx(int lx) {
	this.lx = lx;
}
public int getLy() {
	return ly;
}
public void setLy(int ly) {
	this.ly = ly;
}
	

public void invert() {
	int tx = getHx();
	setHx(getLx());
	setLx(tx);
	int ty = getHy();
	setHy(getLy());
	setLy(ty);
}
public boolean ishl() {
	return getHy() == getLy();
}
public int compareTo(Domino arg0) {
	if (this.getHigh() < arg0.getHigh()) {
		return 1;
	}
	return this.getLow() - arg0.getLow();
}
public int getLow() {
	return low;
}
public void setLow(int low) {
	this.low = low;
}
public int getHx() {
	return hx;
}
public void setHx(int hx) {
	this.hx = hx;
}
public int getHy() {
	return hy;
}
public void setHy(int hy) {
	this.hy = hy;
}
public int getLx() {
	return lx;
}

}