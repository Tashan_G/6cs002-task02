package base;

import static org.junit.jupiter.api.Assertions.*;

import java.io.PrintStream;

import org.junit.jupiter.api.Test;

class MainTest3 {

	@Test
	public void testPg() {
		Main main = new Main();
	        int[][] grid = {
	            {7, 2, 3, 4, 5, 6, 1, 8},
	            {3, 1, 2, 9, 4, 5, 6, 7},
	            {8, 4, 1, 2, 3, 9, 5, 6},
	            {7, 8, 9, 1, 2, 3, 4, 5},
	            {6, 7, 8, 9, 1, 2, 3, 4},
	            {5, 6, 7, 8, 9, 1, 2, 3},
	            {4, 5, 6, 7, 8, 9, 1, 2}
	        };
	        // Redirect standard output to capture the printed output
	        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
	        // Call the pg method
	        int result = main.generategrid(grid);
	        // Restore standard output
	        System.setOut(System.out);	        
	        // Verify the printed output
	        String expectedOutput = "72345618" + System.lineSeparator() +
                    "312.4567" + System.lineSeparator() +
                    "84123.56" + System.lineSeparator() +
                    "78.12345" + System.lineSeparator() +
                    "678.1234" + System.lineSeparator() +
                    "5678.123" + System.lineSeparator() +
                    "45678.12" + System.lineSeparator();
        
	        // Verify the return value
	        assertEquals(11, result);
		}
	}