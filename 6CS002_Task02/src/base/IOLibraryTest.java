package base;

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.io.ByteArrayInputStream;
import java.io.InputStream;

public class IOLibraryTest {

    private final InputStream originalSystemIn = System.in;
    
    @Before
    public void setUpStreams() {
        // Redirect System.in to provide input for testing
        String input = "Test Input\n";
        InputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);
    }

    @After
    public void restoreStreams() {
        // Reset System.in to the original input stream
        System.setIn(originalSystemIn);
    }
    @Test
    public void testGetIPAddress() {
        // Redirects System.in to provide custom input for the method
        assertEquals("Test Input", IOLibrary.getStiring().trim());
    }
}

