package base;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class LocationTest {
	
	@Test
    public void testToStringWithoutDirection() {
        Location location = new Location(2, 3);
        assertEquals("(4,3)", location.toString());
    }

    @Test
    public void testToStringWithDirection() {
        Location location = new Location(2, 3, Location.DIRECTION.VERTICAL);
        assertEquals("(4,3,VERTICAL)", location.toString());
    }

    @Test
    public void testDrawGridLines() {
        
        Location location = new Location(2, 3);
    }

}