package base;
import static org.junit.Assert.*;
import org.junit.Test;

public class DominoTest {

    @Test
    public void testToString() {
        Domino domino = new Domino(1, 2);
        domino.place(1, 2, 3, 4);

    }

    @Test
    public void testAppendPlacedInfo() {
        Domino domino = new Domino(0, 0);
        StringBuffer result = new StringBuffer();
    }

    @Test
    public void testAppendUnplacedInfo() {
        Domino domino = new Domino(0, 0);
        StringBuffer result = new StringBuffer();
 }

    @Test
    public void testSetLx() {
        Domino domino = new Domino(0, 0);
        domino.setLx(10);

        assertEquals(10, domino.getLx());
    }

    @Test
    public void testSetLy() {
        Domino domino = new Domino(0, 0);
        domino.setLy(20);

        assertEquals(20, domino.getLy());
    }

    @Test
    public void testInvert() {
        Domino domino = new Domino(0, 0);
        domino.place(1, 2, 3, 4);
        domino.invert();

        assertEquals(3, domino.getHx());
        assertEquals(4, domino.getHy());
        assertEquals(1, domino.getLx());
        assertEquals(2, domino.getLy());
    }

    @Test
    public void testIshl() {
        Domino domino = new Domino(0, 0);
        domino.place(1, 2, 3, 2);

        assertTrue(domino.ishl());
    }

    @Test
    public void testCompareTo() {
        Domino domino1 = new Domino(0, 0);
        domino1.setHigh(5);
        domino1.setLow(3);

        Domino domino2 = new Domino(0, 0);
        domino2.setHigh(3);
        domino2.setLow(2);

        assertTrue(domino1.compareTo(domino2) > 0);
    }
}
